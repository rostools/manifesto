
# Want to contribute?

Great\! 🎉 Thanks for your interest in improving this project\! At this
stage, we would very much appreciate input on [the ideas laid out in the
manifesto](https://manifesto.rostools.org/). Please open a new issue or
reach out to us on Gitter with your feedback (links below).

In general, the best way to get in touch to discuss new ideas is to
[open a new
issue](https://gitlab.com/rostools/manifesto/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
or reach out via the [Gitter
chat](https://gitter.im/gwas-rostools/lobby). If you want to see what we
are currently working on, you can look at [the issue
list](https://gitlab.com/rostools/manifesto/issues). Remember to follow
the [Code of Conduct](CODE_OF_CONDUCT.md) when you participate in this
project\! :smiley:

## Want to be more involved? Here are more ways to help\!

This document outlines how to contribute to manifesto repository of the
ROStools project. Here are a few ways that you can contribute:

  - Creating a [New
    Issue](https://gitlab.com/rostools/manifesto/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
    within the [manifesto
    repository](https://gitlab.com/rostools/manifesto) on ideas or
    thoughts you may have about the project
  - Check out the [Issues
    Board](https://gitlab.com/rostools/manifesto/-/boards) for an
    overview of what is being worked on and what you could work on that
    others aren’t
  - Participating in discussions on existing
    [Issues](https://gitlab.com/rostools/manifesto/issues)
  - Depending on your interest and level of desired commitment, you can
    try to tackle [`Difficulty:
    Low`](https://gitlab.com/rostools/manifesto/issues?label_name%5B%5D=Difficulty%3A+Low)-labeled
    Issues (or any Issues\!)

### Prerequisites

Before you make a substantial pull request, you should always file an
issue and make sure someone from the team agrees that it’s a problem.

### Pull request process

  - We recommend that you create a Git branch for each merge request
    (MR).
  - We use
    [Markdown](https://rmarkdown.rstudio.com/authoring_basics.html) to
    write the documents as [R
    Markdown](https://rmarkdown.rstudio.com/lesson-1.html) files, then
    we use [bookdown](https://bookdown.org/yihui/bookdown/) to generate
    the website (the website is generated automatically through)

## Submitting

### Code of Conduct

Please note that the manifesto project is released with a [Contributor
Code of Conduct](CODE_OF_CONDUCT.md). By contributing to this project
you agree to abide by its terms.

## Participating in Issue discussions 💭 ❓

Helping to contribute can be as simple as giving feedback or suggestions
or thoughts about a topic or issue. You’ll need a GitLab account to add
your comments to the
[Issues](https://gitlab.com/rostools/manifesto/issues) on the [manifesto
repository](https://gitlab.com/rostools/manifesto). As always, make sure
to adhere to the [Code of Conduct](CODE_OF_CONDUCT.md).

## Adding to the material 📝 💻

The material and website are created using
[bookdown](https://bookdown.org/yihui/bookdown/), which renders the R
Markdown documents and converts the source material into a static
website. The content is written in
[Markdown](https://rmarkdown.rstudio.com/authoring_basics.html) format
in either a Markdown `.md` file or in an [R
Markdown](https://rmarkdown.rstudio.com/lesson-1.html) `.Rmd` file, so
this is the format you would need to use to add to the content.

Anyone can contribute to the course repository via [merge
requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
Please use [the GitLab flow
style](https://docs.gitlab.com/ee/topics/gitlab_flow.html) to manage
changes:

1.  Create a
    [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
    of the repository, and
    [clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
    it to your local computer.
2.  In your local copy of this repository, create a new
    [branch](https://docs.gitlab.com/ee/user/project/repository/branches/#branches).
3.  Commit your changes to that branch.
4.  Push the edits on that branch to your fork of the course repository.
5.  Submit a pull request to the master repository
    (`rostools/manifesto`).
6.  If you receive feedback on your pull request, make further commits
    to the new branch on your fork. These will automatically be added to
    your pull request.

## Acknowledgements

Parts of the file were modified from the
[UofTCoders](https://github.com/UofTCoders/studyGroup/blob/gh-pages/CONTRIBUTING.md)
contributing guidelines.
