# Contributor Code of Conduct

This is a compilation of what we consider to be the key points from the
[Mozilla], [Jupyter], [Contributor Covenant], [Ubuntu] community guidelines.
This isn't an exhaustive list of things that you can or can't do. Rather,
it aims to provide general guidelines for successfully conducting collaborative
work in an exciting and fun environment. Please report unacceptable behavior via
a private message to a project maintainer in [our gitter channel](https://gitter.im/gwas-rostools/lobby).
As contributors and maintainers of this project, we pledge to respect all people who 
contribute through reporting issues, posting feature requests, updating documentation,
submitting pull requests or patches, and other activities.

[Jupyter]: https://github.com/jupyter/governance/blob/master/conduct/code_of_conduct.md
[Mozilla]: https://www.mozilla.org/en-US/about/governance/policies/participation/
[Ubuntu]: https://www.ubuntu.com/community/code-of-conduct
[Contributor Covenant]: https://www.contributor-covenant.org

- **Be welcoming, friendly, and patient.**
    - We strive to and are committed to making participation in this project a
      harassment-free experience for everyone, regardless of level of experience,
      gender, gender identity and expression, sexual orientation, disability, personal
      appearance, body size, race, ethnicity, age, or religion.
      Remember that you can interact with people from all over the world and
      that you may be communicating with someone with a different primary
      language or cultural background.

- **Be direct and respectful.**
    -  We must be able to speak directly when we disagree and when we think we
       need to improve. We cannot withhold hard truths. Doing so respectfully is
       hard, doing so when others don’t seem to be listening is harder, and
       hearing such comments when one is the recipient can be even harder still.
       We need to be honest and direct, as well as respectful

- **Understand and learn from disagreement and different perspectives.**
    - Our goal should not be to personally “win” every disagreement or argument.
      Value discussion and be open to ideas that make our own ideas better,
      while also making sure to speak up when and we disagree with an idea and
      explain why. Being unable to understand why someone holds a viewpoint
      doesn’t mean that they’re wrong. Don’t forget that it is human to err and
      blaming each other doesn’t get us anywhere, rather offer to help resolving
      issues and to help learn from mistakes. Have a dialectic, not a debate.
      Actual winning is when different perspectives make our work richer and
      stronger.

- **Lead by example.**
    - By matching your actions with your words, you become a person others want
      to follow. Your actions influence others to behave and respond in ways
      that are valuable and appropriate for our organizational outcomes. Design
      your community and your work for inclusion. Hold yourself and others
      accountable for inclusive behaviors.

- **Ask for help when unsure.**
    - Nobody is expected to be perfect. Asking questions early
      avoids many problems later, so questions are encouraged, though they may
      be directed to the appropriate forum. Those who are asked should be
      responsive and helpful.

- **Give people the benefit of the doubt.**
    - Ask for clarification instead of jumping to conclusions.

- **A simple apology can go a long way.**
    - It can often de-escalate a situation, and telling someone that you are
      sorry is an act of empathy that doesn’t automatically imply an admission
      of guilt.

- **Be considerate in the words that you choose.**
    - Do not insult or put down other community members. Harassment and other
      exclusionary behavior are not acceptable. This includes, but is not
      limited to
      -  Violent threats or violent language directed against another person
      -  Discriminatory jokes and language
      -  Posting sexually explicit or violent material
      -  Posting (or threatening to post) other people's personally identifying
         information ("doxing")
      -  Personal insults, especially those using racist or sexist terms
      -  Unwelcome sexual attention
      -  Advocating for, or encouraging, any of the above behavior
      -  Repeated harassment of others. In general, if someone asks you to stop,
         then stop.

Project maintainers have the right and responsibility to remove, edit, or reject comments,
commits, code, wiki edits, issues, and other contributions that are not aligned to this 
Code of Conduct. Project maintainers who do not follow the Code of Conduct may be removed 
from the project team.

This code of conduct is released under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
