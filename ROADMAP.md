# ROADMAP

## Project mission and summary

Welcome to the rostools project! 

Our dream is a future where the default for science is to be reproducible and
open. We hope that one day, researchers will conduct their scientific activities
following open scientific principles not as an active choice but because it is
the proper and *easier* way of doing science.

To achieve our dream, our mission is to create a highly opinionated, practical,
and process-oriented manifesto and an accompanying ecosystem of software tools,
documentation, tutorials, and learning materials, supported by a network of
practitioners (researchers), that informs researchers on how to conduct open and
reproducible science.

See the [overview](index.Rmd) for more description about the project's vision and mission.
If you are interested in getting involved,
check out our [contributing guidelines](CONTRIBUTING.md) for more details.

## Milestones and tasks

Our main priority is to finish the **first working version** of the manifesto.
All tasks related to completing our milestones are listed in GitLab Issues and 
can be viewed on the [Issue Board](https://gitlab.com/rostools/manifesto/-/boards).
Issues labeled with [`Priority: High`](https://gitlab.com/rostools/manifesto/issues?label_name%5B%5D=Priority%3A+High)
or with [`Content`](https://gitlab.com/rostools/manifesto/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Content)
are good issues to work on if they aren't already assigned.
