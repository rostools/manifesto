# manifesto

Contains the opinionated manifesto for conducting a reproducible and open scientific data analysis.

## Contributing

Interested in contributing to the project? 
Check out our [contributing guidelines](CONTRIBUTING.md) for more details!
Please note that the 'manifesto' project is released with a
[Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By contributing to this project, you agree to abide by its terms.
